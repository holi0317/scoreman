import "./bootstrap.scss";

import Alert from "bootstrap/js/src/alert";
Array.from(document.querySelectorAll(".alert")).forEach(
  (node) => new Alert(node)
);

import Button from "bootstrap/js/src/button";
Array.from(document.querySelectorAll(".btn")).forEach(
  (node) => new Button(node)
);

// import Carousel from "bootstrap/js/src/carousel";
// import Collapse from "bootstrap/js/src/collapse";
// import Dropdown from "bootstrap/js/src/dropdown";
// import Modal from "bootstrap/js/src/modal";
// import Offcanvas from "bootstrap/js/src/offcanvas";
// import Popover from "bootstrap/js/src/popover";
// import Scrollspy from "bootstrap/js/src/scrollspy";
// import Tab from "bootstrap/js/src/tab";
// import Toast from "bootstrap/js/src/toast";
// import Tooltip from "bootstrap/js/src/tooltip";
