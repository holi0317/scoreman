import { defineConfig } from "vite";

// https://vitejs.dev/config/
export default defineConfig({
  clearScreen: false,
  build: {
    outDir: "base/static/",
    emptyOutDir: false,
    manifest: true,
    rollupOptions: {
      input: "web/index.js",
    },
  },
  server: {
    strictPort: true,
  },
});
