const e = {
    find: (e, t = document.documentElement) =>
      [].concat(...Element.prototype.querySelectorAll.call(t, e)),
    findOne: (e, t = document.documentElement) =>
      Element.prototype.querySelector.call(t, e),
    children: (e, t) => [].concat(...e.children).filter((e) => e.matches(t)),
    parents(e, t) {
      const n = [];
      let r = e.parentNode;
      for (; r && r.nodeType === Node.ELEMENT_NODE && 3 !== r.nodeType; )
        r.matches(t) && n.push(r), (r = r.parentNode);
      return n;
    },
    prev(e, t) {
      let n = e.previousElementSibling;
      for (; n; ) {
        if (n.matches(t)) return [n];
        n = n.previousElementSibling;
      }
      return [];
    },
    next(e, t) {
      let n = e.nextElementSibling;
      for (; n; ) {
        if (n.matches(t)) return [n];
        n = n.nextElementSibling;
      }
      return [];
    },
  },
  t = () => {
    const { jQuery: e } = window;
    return e && !document.body.hasAttribute("data-bs-no-jquery") ? e : null;
  },
  n = [],
  r = (e) => {
    var r;
    (r = () => {
      const n = t();
      if (n) {
        const t = e.NAME,
          r = n.fn[t];
        (n.fn[t] = e.jQueryInterface),
          (n.fn[t].Constructor = e),
          (n.fn[t].noConflict = () => ((n.fn[t] = r), e.jQueryInterface));
      }
    }),
      "loading" === document.readyState
        ? (n.length ||
            document.addEventListener("DOMContentLoaded", () => {
              n.forEach((e) => e());
            }),
          n.push(r))
        : r();
  },
  o = (e) => {
    "function" == typeof e && e();
  },
  s = (e, t, n = !0) => {
    if (!n) return void o(e);
    const r =
      ((e) => {
        if (!e) return 0;
        let { transitionDuration: t, transitionDelay: n } =
          window.getComputedStyle(e);
        const r = Number.parseFloat(t),
          o = Number.parseFloat(n);
        return r || o
          ? ((t = t.split(",")[0]),
            (n = n.split(",")[0]),
            1e3 * (Number.parseFloat(t) + Number.parseFloat(n)))
          : 0;
      })(t) + 5;
    let s = !1;
    const i = ({ target: n }) => {
      n === t && ((s = !0), t.removeEventListener("transitionend", i), o(e));
    };
    t.addEventListener("transitionend", i),
      setTimeout(() => {
        s || t.dispatchEvent(new Event("transitionend"));
      }, r);
  },
  i = /[^.]*(?=\..*)\.|.*/,
  l = /\..*/,
  a = /::\d+$/,
  c = {};
let u = 1;
const d = { mouseenter: "mouseover", mouseleave: "mouseout" },
  f = /^(mouseenter|mouseleave)/i,
  g = new Set([
    "click",
    "dblclick",
    "mouseup",
    "mousedown",
    "contextmenu",
    "mousewheel",
    "DOMMouseScroll",
    "mouseover",
    "mouseout",
    "mousemove",
    "selectstart",
    "selectend",
    "keydown",
    "keypress",
    "keyup",
    "orientationchange",
    "touchstart",
    "touchmove",
    "touchend",
    "touchcancel",
    "pointerdown",
    "pointermove",
    "pointerup",
    "pointerleave",
    "pointercancel",
    "gesturestart",
    "gesturechange",
    "gestureend",
    "focus",
    "blur",
    "change",
    "reset",
    "select",
    "submit",
    "focusin",
    "focusout",
    "load",
    "unload",
    "beforeunload",
    "resize",
    "move",
    "DOMContentLoaded",
    "readystatechange",
    "error",
    "abort",
    "scroll",
  ]);
function m(e, t) {
  return (t && `${t}::${u++}`) || e.uidEvent || u++;
}
function h(e) {
  const t = m(e);
  return (e.uidEvent = t), (c[t] = c[t] || {}), c[t];
}
function p(e, t, n = null) {
  const r = Object.keys(e);
  for (let o = 0, s = r.length; o < s; o++) {
    const s = e[r[o]];
    if (s.originalHandler === t && s.delegationSelector === n) return s;
  }
  return null;
}
function E(e, t, n) {
  const r = "string" == typeof t,
    o = r ? n : t;
  let s = b(e);
  return g.has(s) || (s = e), [r, o, s];
}
function v(e, t, n, r, o) {
  if ("string" != typeof t || !e) return;
  if ((n || ((n = r), (r = null)), f.test(t))) {
    const e = (e) =>
      function (t) {
        if (
          !t.relatedTarget ||
          (t.relatedTarget !== t.delegateTarget &&
            !t.delegateTarget.contains(t.relatedTarget))
        )
          return e.call(this, t);
      };
    r ? (r = e(r)) : (n = e(n));
  }
  const [s, l, a] = E(t, n, r),
    c = h(e),
    u = c[a] || (c[a] = {}),
    d = p(u, l, s ? n : null);
  if (d) return void (d.oneOff = d.oneOff && o);
  const g = m(l, t.replace(i, "")),
    v = s
      ? (function (e, t, n) {
          return function r(o) {
            const s = e.querySelectorAll(t);
            for (let { target: i } = o; i && i !== this; i = i.parentNode)
              for (let l = s.length; l--; )
                if (s[l] === i)
                  return (
                    (o.delegateTarget = i),
                    r.oneOff && _.off(e, o.type, t, n),
                    n.apply(i, [o])
                  );
            return null;
          };
        })(e, n, r)
      : (function (e, t) {
          return function n(r) {
            return (
              (r.delegateTarget = e),
              n.oneOff && _.off(e, r.type, t),
              t.apply(e, [r])
            );
          };
        })(e, n);
  (v.delegationSelector = s ? n : null),
    (v.originalHandler = l),
    (v.oneOff = o),
    (v.uidEvent = g),
    (u[g] = v),
    e.addEventListener(a, v, s);
}
function y(e, t, n, r, o) {
  const s = p(t[n], r, o);
  s && (e.removeEventListener(n, s, Boolean(o)), delete t[n][s.uidEvent]);
}
function b(e) {
  return (e = e.replace(l, "")), d[e] || e;
}
const _ = {
    on(e, t, n, r) {
      v(e, t, n, r, !1);
    },
    one(e, t, n, r) {
      v(e, t, n, r, !0);
    },
    off(e, t, n, r) {
      if ("string" != typeof t || !e) return;
      const [o, s, i] = E(t, n, r),
        l = i !== t,
        c = h(e),
        u = t.startsWith(".");
      if (void 0 !== s) {
        if (!c || !c[i]) return;
        return void y(e, c, i, s, o ? n : null);
      }
      u &&
        Object.keys(c).forEach((n) => {
          !(function (e, t, n, r) {
            const o = t[n] || {};
            Object.keys(o).forEach((s) => {
              if (s.includes(r)) {
                const r = o[s];
                y(e, t, n, r.originalHandler, r.delegationSelector);
              }
            });
          })(e, c, n, t.slice(1));
        });
      const d = c[i] || {};
      Object.keys(d).forEach((n) => {
        const r = n.replace(a, "");
        if (!l || t.includes(r)) {
          const t = d[n];
          y(e, c, i, t.originalHandler, t.delegationSelector);
        }
      });
    },
    trigger(e, n, r) {
      if ("string" != typeof n || !e) return null;
      const o = t(),
        s = b(n),
        i = n !== s,
        l = g.has(s);
      let a,
        c = !0,
        u = !0,
        d = !1,
        f = null;
      return (
        i &&
          o &&
          ((a = o.Event(n, r)),
          o(e).trigger(a),
          (c = !a.isPropagationStopped()),
          (u = !a.isImmediatePropagationStopped()),
          (d = a.isDefaultPrevented())),
        l
          ? ((f = document.createEvent("HTMLEvents")), f.initEvent(s, c, !0))
          : (f = new CustomEvent(n, { bubbles: c, cancelable: !0 })),
        void 0 !== r &&
          Object.keys(r).forEach((e) => {
            Object.defineProperty(f, e, { get: () => r[e] });
          }),
        d && f.preventDefault(),
        u && e.dispatchEvent(f),
        f.defaultPrevented && void 0 !== a && a.preventDefault(),
        f
      );
    },
  },
  A = new Map();
var O = {
  set(e, t, n) {
    A.has(e) || A.set(e, new Map());
    const r = A.get(e);
    r.has(t) || 0 === r.size
      ? r.set(t, n)
      : console.error(
          `Bootstrap doesn't allow more than one instance per element. Bound instance: ${
            Array.from(r.keys())[0]
          }.`
        );
  },
  get: (e, t) => (A.has(e) && A.get(e).get(t)) || null,
  remove(e, t) {
    if (!A.has(e)) return;
    const n = A.get(e);
    n.delete(t), 0 === n.size && A.delete(e);
  },
};
class w {
  constructor(t) {
    var n;
    (t = ((e) =>
      !(!e || "object" != typeof e) &&
      (void 0 !== e.jquery && (e = e[0]), void 0 !== e.nodeType))((n = t))
      ? n.jquery
        ? n[0]
        : n
      : "string" == typeof n && n.length > 0
      ? e.findOne(n)
      : null) &&
      ((this._element = t),
      O.set(this._element, this.constructor.DATA_KEY, this));
  }
  dispose() {
    O.remove(this._element, this.constructor.DATA_KEY),
      _.off(this._element, this.constructor.EVENT_KEY),
      Object.getOwnPropertyNames(this).forEach((e) => {
        this[e] = null;
      });
  }
  _queueCallback(e, t, n = !0) {
    s(e, t, n);
  }
  static getInstance(e) {
    return O.get(e, this.DATA_KEY);
  }
  static getOrCreateInstance(e, t = {}) {
    return this.getInstance(e) || new this(e, "object" == typeof t ? t : null);
  }
  static get VERSION() {
    return "5.0.2";
  }
  static get NAME() {
    throw new Error(
      'You have to implement the static method "NAME", for each component!'
    );
  }
  static get DATA_KEY() {
    return `bs.${this.NAME}`;
  }
  static get EVENT_KEY() {
    return `.${this.DATA_KEY}`;
  }
}
class N extends w {
  static get NAME() {
    return "alert";
  }
  close(e) {
    const t = e ? this._getRootElement(e) : this._element,
      n = this._triggerCloseEvent(t);
    null === n || n.defaultPrevented || this._removeElement(t);
  }
  _getRootElement(e) {
    return (
      ((e) => {
        const t = ((e) => {
          let t = e.getAttribute("data-bs-target");
          if (!t || "#" === t) {
            let n = e.getAttribute("href");
            if (!n || (!n.includes("#") && !n.startsWith("."))) return null;
            n.includes("#") &&
              !n.startsWith("#") &&
              (n = `#${n.split("#")[1]}`),
              (t = n && "#" !== n ? n.trim() : null);
          }
          return t;
        })(e);
        return t ? document.querySelector(t) : null;
      })(e) || e.closest(".alert")
    );
  }
  _triggerCloseEvent(e) {
    return _.trigger(e, "close.bs.alert");
  }
  _removeElement(e) {
    e.classList.remove("show");
    const t = e.classList.contains("fade");
    this._queueCallback(() => this._destroyElement(e), e, t);
  }
  _destroyElement(e) {
    e.remove(), _.trigger(e, "closed.bs.alert");
  }
  static jQueryInterface(e) {
    return this.each(function () {
      const t = N.getOrCreateInstance(this);
      "close" === e && t[e](this);
    });
  }
  static handleDismiss(e) {
    return function (t) {
      t && t.preventDefault(), e.close(this);
    };
  }
}
_.on(
  document,
  "click.bs.alert.data-api",
  '[data-bs-dismiss="alert"]',
  N.handleDismiss(new N())
),
  r(N);
class T extends w {
  static get NAME() {
    return "button";
  }
  toggle() {
    this._element.setAttribute(
      "aria-pressed",
      this._element.classList.toggle("active")
    );
  }
  static jQueryInterface(e) {
    return this.each(function () {
      const t = T.getOrCreateInstance(this);
      "toggle" === e && t[e]();
    });
  }
}
_.on(document, "click.bs.button.data-api", '[data-bs-toggle="button"]', (e) => {
  e.preventDefault();
  const t = e.target.closest('[data-bs-toggle="button"]');
  T.getOrCreateInstance(t).toggle();
}),
  r(T);
export { N as A, T as B };
