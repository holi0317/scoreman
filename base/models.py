from django.db.models import TextField
from django.forms import TextInput
from django.contrib.admin.widgets import AdminTextareaWidget, AdminTextInputWidget


class ShortTextField(TextField):
    """Same as :py:class:`django.db.models.TextField`, but uses ``<input type="text">`` for form input."""

    def formfield(self, **kwargs):
        kwargs.setdefault("max_length", self.max_length)

        if "widget" in kwargs and issubclass(kwargs["widget"], AdminTextareaWidget):
            kwargs["widget"] = AdminTextInputWidget
        if self.choices is None:
            kwargs.setdefault("widget", TextInput)

        return super().formfield(**kwargs)
