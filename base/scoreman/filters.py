from django.db.models import Q
from django_filters import FilterSet
from django_filters.filters import CharFilter

from .forms import InlineFilterForm
from .models import Sheet


class SheetFilter(FilterSet):
    name = CharFilter(method="filter_name")

    def filter_name(self, queryset, name, value):
        return queryset.filter(Q(name__icontains=value) | Q(alias__icontains=value))

    class Meta:
        model = Sheet
        fields = ["name", "composer", "arranger", "group", "tags"]
        form = InlineFilterForm
