from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
from django.views.generic import ListView
from django_filters.views import FilterView
from django_tables2.views import SingleTableMixin
from crispy_forms.helper import FormHelper

from base.scoreman.models import Sheet, SheetTag
from .tables import SheetTable
from .filters import SheetFilter


def index(request: HttpRequest) -> HttpResponse:
    sheet_count = Sheet.objects.count()

    context = {"sheet_count": sheet_count}
    return render(request, "scoreman/index.html", context)


class SheetListView(SingleTableMixin, FilterView):
    model = Sheet
    filterset_class = SheetFilter
    table_class = SheetTable
    template_name = "scoreman/sheet_list.html"


class SheetTagListView(ListView):
    model = SheetTag
    template_name = "scoreman/sheettag_list.html"
