from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django.forms import Form


class InlineFilterForm(Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_method = "GET"
        self.helper.form_action = ""

        self.helper.form_class = "form-inline"
        self.helper.form_class = "row row-cols-lg-auto"
        self.helper.field_class = "col-auto"
        self.helper.field_template = "bootstrap5/layout/inline_field.html"
        self.helper.add_input(Submit("submit", "Search"))
