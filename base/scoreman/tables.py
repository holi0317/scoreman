import django_tables2 as tables
from .models import Sheet


class SheetTable(tables.Table):
    class Meta:
        model = Sheet
        template_name = "django_tables2/bootstrap4.html"
        fields = (
            "name",
            "composer",
            "group",
            "tags",
        )
