from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("sheet", views.SheetListView.as_view(), name="sheet_list"),
    path("sheettag", views.SheetTagListView.as_view(), name="sheettag_list"),
]
