from django.db import models
from django.db.models.fields import SmallIntegerField
from django.utils import timezone
from django.core.validators import MinValueValidator
from django.contrib.humanize.templatetags.humanize import ordinal
from filer.fields.file import FilerFileField

from base.models import ShortTextField


class Instrument(models.Model):
    class Family(models.TextChoices):
        CONDUCTOR = "Conductor", "Conductor"
        STRINGS = "Strings", "Strings"
        WOODWINDS = "Woodwinds", "Woodwinds"
        BRASS = "Brass", "Brass"
        PERCUSSION = "Percussion", "Percussion"

    id = models.BigAutoField(primary_key=True)

    name = models.TextField(null=False, unique=True)
    family = models.TextField(null=True, choices=Family.choices)

    def __str__(self) -> str:
        return self.name


class SheetTag(models.Model):
    id = models.BigAutoField(primary_key=True)

    name = models.TextField(null=False, unique=True)
    color = models.TextField(null=False)

    def __str__(self) -> str:
        return self.name


class Person(models.Model):
    """A person that involves in creating the sheet music."""

    id = models.BigAutoField(primary_key=True)

    name = models.TextField(null=False, unique=True)
    alias = models.TextField(null=False, blank=True, default="")

    def __str__(self) -> str:
        if self.alias != "":
            return f"{self.name} ({self.alias})"
        return self.name


class Sheet(models.Model):
    """A sheet music. Could also be interpreted as a song/piece."""

    class Group(models.TextChoices):
        CONCERT_BAND = "CONCERT_BAND", "Concert Band"
        ORCHESTRA = "ORCHESTRA", "Orchestra"
        BRASS_ENSEMBLE = "BRASS_ENSEMBLE", "Brass Ensemble"
        JAZZ_BAND = "JAZZ_BAND", "Jazz Band"

    id = models.BigAutoField(primary_key=True)

    tags = models.ManyToManyField(SheetTag, blank=True)

    name = ShortTextField(
        null=False,
        help_text="Name of the sheet in native language. For alternative name, use alias field instead",
    )
    composer = models.ForeignKey(
        Person,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="composer_of",
    )
    arranger = models.ForeignKey(
        Person,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="arranger_of",
    )
    alias = ShortTextField(
        null=False,
        blank=True,
        default="",
        help_text="Alternative name (or nickname) of this sheet",
    )

    create_date = models.DateTimeField(
        null=False,
        default=timezone.now,
        help_text="Date for adding this sheet into the system",
    )

    group = models.TextField(
        null=False, choices=Group.choices, default=Group.CONCERT_BAND
    )

    source = models.URLField(
        null=False,
        blank=True,
        default="",
        help_text="Buy link or download link of the sheet",
    )

    comment = models.TextField(
        null=False, blank=True, default="", help_text="Additional notes for this sheet"
    )

    def __str__(self) -> str:
        return self.name


class LogicalFile(models.Model):
    """LogicalFile represents a part of a file.

    This model mostly for PDF files. Where we want to express a subset of a pdf file
    while keeping the original pdf file untouched.

    For other file types, this model have no use.
    """

    id = models.BigAutoField(primary_key=True)

    file = FilerFileField(
        null=False, related_name="logical_file", on_delete=models.CASCADE
    )

    pages = models.TextField(
        null=False,
        blank=True,
        default="",
        help_text="Page range for this file. Only valid if the target file is pdf. Empty means select all pages",
    )

    def __str__(self) -> str:
        return str(self.file)


class Part(models.Model):
    id = models.BigAutoField(primary_key=True)

    instrument = models.ForeignKey(Instrument, on_delete=models.PROTECT)
    sheet = models.ForeignKey(Sheet, on_delete=models.CASCADE)
    files = models.ManyToManyField(LogicalFile, through="PartFile")

    alias = ShortTextField(
        null=False,
        blank=True,
        default="",
        help_text="Alias for this part. eg offstage trumpet, bass trombone",
    )

    part_num = models.IntegerField(
        default=0,
        null=False,
        validators=[MinValueValidator(0)],
        help_text="Part number. EG oboe 1st this field should be 1. Use 0 for part that has no part number",
    )

    def __str__(self) -> str:
        if self.alias != "":
            return self.alias

        if self.part_num == 0:
            return self.instrument.name

        return f"{self.instrument.name} {ordinal(self.part_num)}"

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["sheet", "instrument", "part_num"], name="unique_part_per_sheet"
            )
        ]


class PartFile(models.Model):
    """Through table for ``Part`` and ``LogicalFile``."""

    id = models.BigAutoField(primary_key=True)

    file = models.ForeignKey(LogicalFile, on_delete=models.CASCADE)
    part = models.ForeignKey(Part, on_delete=models.CASCADE)

    seq = models.IntegerField(null=False, default=0)
