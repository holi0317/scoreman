from django.apps import AppConfig


class ScoremanConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "base.scoreman"
