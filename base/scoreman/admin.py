from django.contrib import admin
from django.db.models.aggregates import Count
from django.db.models.query import QuerySet
from django.http.request import HttpRequest
from . import models


class PartInline(admin.TabularInline):
    model = models.Part
    extra = 1
    show_change_link = True

    readonly_fields = ("logical_files_count",)

    def get_queryset(self, request: HttpRequest) -> QuerySet:
        qs = super().get_queryset(request)
        return qs.annotate(files_count=Count("files"))

    @admin.display(description="Number of related files")
    def logical_files_count(self, instance: models.Part) -> str:
        return str(instance.files_count)


class PartFileInline(admin.TabularInline):
    model = models.PartFile
    extra = 1


@admin.register(models.LogicalFile)
class LogicalFileAdmin(admin.ModelAdmin):
    list_select_related = ("file",)


@admin.register(models.Sheet)
class SheetAdmin(admin.ModelAdmin):
    inlines = (PartInline,)


@admin.register(models.Person)
class PersonAdmin(admin.ModelAdmin):
    pass


@admin.register(models.SheetTag)
class SheetTagAdmin(admin.ModelAdmin):
    pass


@admin.register(models.Instrument)
class InstrumentAdmin(admin.ModelAdmin):
    search_fields = ("name",)


@admin.register(models.Part)
class PartAdmin(admin.ModelAdmin):
    list_display = ("sheet", "instrument", "part_num", "logical_files_count")
    list_select_related = ("sheet", "instrument")
    search_fields = ("sheet__name", "instrument__name")
    inlines = (PartFileInline,)

    def get_queryset(self, request: HttpRequest) -> QuerySet:
        qs = super().get_queryset(request)
        return qs.annotate(files_count=Count("files"))

    @admin.display(description="Number of related files")
    def logical_files_count(self, instance: models.Part) -> str:
        return str(instance.files_count)
